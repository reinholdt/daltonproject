import numpy as np
import daltonproject as dp

dalton = dp.QCProgram(program='Dalton')
lsdalton = dp.QCProgram(program='LSDalton')

qc_methods = [dp.QCMethod('HF'), dp.QCMethod('DFT', 'B3LYP'), dp.QCMethod('DFT', 'CAMB3LYP')]
basis_sets = [dp.Basis(basis=basis) for basis in ['STO-3G', 'pcseg-0', '4-31G']]
water = dp.Molecule(xyz='../sample_molecules/water.xyz', charge=0)
energy = dp.Property(energy=True)

# WARNING this will create a lot of input and output files
dalton_results = []
lsdalton_results = []
for qc_method in qc_methods:
    for basis_set in basis_sets:
        dalton_results.append(dalton.compute(qc_method, basis_set, water, energy))
        lsdalton_results.append(lsdalton.compute(qc_method, basis_set, water, energy))

dalton_outputs = []
for result in dalton_results:
    dalton_outputs.append(dp.PostProcess(dalton, result))

lsdalton_outputs = []
for result in lsdalton_results:
    lsdalton_outputs.append(dp.PostProcess(lsdalton, result))

for dalton_output, lsdalton_output in zip(dalton_outputs, lsdalton_outputs):
    dalton_energy = dalton_output.energy
    lsdalton_energy = lsdalton_output.energy
    print(dalton_energy, lsdalton_energy, f'is equal? {round(dalton_energy, 5) == round(lsdalton_energy, 5)}')
