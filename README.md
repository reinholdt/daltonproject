# Dalton Project

The Dalton Project is a platform for quantum chemistry codes
...
## Installation

Download

    $ git clone https://gitlab.com/DaltonPlatform/platform-demo

Create and activate a virtual environment (python version >= 3.6)

    $ virtualenv venv -p python3.6
    $ source venv/bin/activate
    $ pip install -r requirements.txt

For the above, you may need to first install virtualenv (requires Python 3)

    $ pip3 install virtualenv

Add to your PATH and run tests

    $ python -m pytest
    ============================= test session starts ==============================
    platform linux -- Python 3.6.7, pytest-4.1.1, py-1.7.0, pluggy-0.8.1
    rootdir: /tmp/dalton-project, inifile:
    collected 14 items                                                             

    tests/test_dalton_ifc.py .....                                           [ 35%]
    tests/test_scf.py ..                                                     [ 50%]
    tests/test_xyz2mol.py .......                                            [100%]

    ========================== 14 passed in 1.84 seconds ===========================
    

## Example

    >>> import daltonproject as dp
    >>> prg = dp.set_code('dalton')
    >>> prg.run_scf('HF/STO-3G', 'sample_molecules/ethanol.xyz')
    >>> prg.get_energy()
    -152.129820186863



