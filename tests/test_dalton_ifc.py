import pytest
import daltonproject.dalton_ifc as dalton_ifc


def test_get_atom_basis():
    atom_basis = dalton_ifc.get_atom_basis(basis='STO-3G', num_atoms=3, labels=['O1', 'H1', 'H2'])
    assert atom_basis == ['STO-3G', 'STO-3G', 'STO-3G']
    atom_basis = dalton_ifc.get_atom_basis(basis={'H1': 'STO-3G', 'H2': 'STO-3G', 'O1': '6-31G*'}, num_atoms=3,
                                           labels=['O1', 'H1', 'H2'])
    assert atom_basis == ['6-31G*', 'STO-3G', 'STO-3G']


def test_get_atom_basis_exceptions():
    with pytest.raises(KeyError):
        dalton_ifc.get_atom_basis(basis={'H1': 'STO-3G', 'H2': 'STO-3G'}, num_atoms=3, labels=['O1', 'H1', 'H2'])
