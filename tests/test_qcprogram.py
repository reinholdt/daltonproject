import pytest
import daltonproject as dp
from daltonproject.dalton_ifc import DaltonIFC
from daltonproject.lsdalton_ifc import LSDaltonIFC


def test_qcprogram_dalton():
    dalton = dp.QCProgram('Dalton')
    assert isinstance(dalton, DaltonIFC)


def test_qcprogram_lsdalton():
    lsdalton = dp.QCProgram('LSDalton')
    assert isinstance(lsdalton, LSDaltonIFC)


def test_qcprogram_exceptions():
    with pytest.raises(TypeError):
        dp.QCProgram(['Dalton'])
    with pytest.raises(ValueError):
        dp.QCProgram('Dirac')
