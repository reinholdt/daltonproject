import pytest
import daltonproject as dp


def test_qcmethod_hf():
    test = dp.QCMethod("hf")
    assert isinstance(test.settings, dict)
    assert test.settings['qc_method'] == 'HF'


def test_qcmethod_dft():
    test = dp.QCMethod(qc_method="DFT", xc_functional="b3lyp")
    assert isinstance(test.settings, dict)
    assert test.settings['qc_method'] == 'DFT'
    assert test.settings['xc_functional'] == 'B3LYP'
    test = dp.QCMethod(qc_method="DFT")
    test.xc_functional("B3LYP")
    assert test.settings['qc_method'] == 'DFT'
    assert test.settings['xc_functional'] == 'B3LYP'


def test_qcmethod_exception():
    with pytest.raises(TypeError):
        dp.QCMethod(["HF"])
    with pytest.raises(TypeError):
        dp.QCMethod(qc_method="DFT", xc_functional=["B3LYP"])
    with pytest.raises(TypeError):
        dp.QCMethod(qc_method="HF", xc_functional="B3LYP")
    with pytest.raises(TypeError):
        dp.QCMethod()
    with pytest.raises(TypeError):
        test = dp.QCMethod("HF")
        test.scf_threshold("1e-6")


def test_qcmethod_scf_threshold():
    test = dp.QCMethod("HF")
    test.scf_threshold(1e-6)
    assert pytest.approx(test.settings['scf_threshold'], 1e-6)
