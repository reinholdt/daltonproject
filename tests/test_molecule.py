import os
import pytest
import numpy as np
import daltonproject as dp


def test_molecule_init_string():
    # init molecule with atoms given as a string using ";" as atom separator
    ref_elements = ['O', 'H', 'H']
    ref_coordinates = [[1.0, 2.0, 0.5], [0.0, 1.0, 1.0], [-1.0, 0.5, 4.0]]
    atoms = "O 1.0 2.0 0.5; H 0.0 1.0 1.0; H -1.0 0.5 4.0"
    molecule = dp.Molecule(atoms=atoms)
    assert isinstance(molecule.charge, int)
    assert molecule.charge == 0
    assert isinstance(molecule.elements, list)
    for element in molecule.elements:
        assert isinstance(element, str)
    assert molecule.elements == ref_elements
    assert molecule.labels == ref_elements
    assert isinstance(molecule.coordinates, np.ndarray)
    for coordinate in molecule.coordinates:
        assert isinstance(coordinate, np.ndarray)
        for value in coordinate:
            assert isinstance(value, float)
    for test_coordinate, ref_coordinate in zip(molecule.coordinates, ref_coordinates):
        assert pytest.approx(test_coordinate.tolist()) == ref_coordinate
    # init molecule with a single atom
    ref_elements = ['O']
    ref_coordinates = [[1.0, 2.0, 0.5]]
    atom = "O 1.0 2.0 0.5"
    molecule = dp.Molecule(atoms=atom)
    assert isinstance(molecule.charge, int)
    assert molecule.charge == 0
    assert isinstance(molecule.elements, list)
    for element in molecule.elements:
        assert isinstance(element, str)
    assert molecule.elements == ref_elements
    assert molecule.labels == ref_elements
    assert isinstance(molecule.coordinates, np.ndarray)
    for coordinate in molecule.coordinates:
        assert isinstance(coordinate, np.ndarray)
        for value in coordinate:
            assert isinstance(value, float)
    for test_coordinate, ref_coordinate in zip(molecule.coordinates, ref_coordinates):
        assert pytest.approx(test_coordinate.tolist()) == ref_coordinate
    # init molecule with atoms given as a string using "\n" as atom separator and adding atom labels
    ref_elements = ['O', 'H', 'H']
    ref_names = ['O1', 'H1', 'H2']
    ref_coordinates = [[1.0, 2.0, 0.5], [0.0, 1.0, 1.0], [-1.0, 0.5, 4.0]]
    atoms = "O 1.0 2.0 0.5 O1\n H 0.0 1.0 1.0 H1\n H -1.0 0.5 4.0 H2"
    molecule = dp.Molecule(atoms=atoms, charge=-1)
    assert isinstance(molecule.charge, int)
    assert molecule.charge == -1
    assert isinstance(molecule.elements, list)
    for element in molecule.elements:
        assert isinstance(element, str)
    assert molecule.elements == ref_elements
    assert molecule.labels == ref_names
    assert isinstance(molecule.coordinates, np.ndarray)
    for coordinate in molecule.coordinates:
        assert isinstance(coordinate, np.ndarray)
        for value in coordinate:
            assert isinstance(value, float)
    for test_coordinate, ref_coordinate in zip(molecule.coordinates, ref_coordinates):
        assert pytest.approx(test_coordinate.tolist()) == ref_coordinate


def test_molecule_init_atoms_xyz():
    ref_elements = ['C', 'H', 'H', 'H', 'C', 'H', 'H', 'O', 'H']
    ref_names = ['C1', 'H1', 'H2', 'H3', 'C2', 'H4', 'H5', 'O', 'H6']
    ref_coordinates = [[ 9.136479, 11.724970, 51.500927],
                       [ 9.294462, 12.628635, 50.910741],
                       [ 8.160480, 11.311733, 51.245960],
                       [ 9.126987, 11.998458, 52.556137],
                       [10.230421, 10.715737, 51.222941],
                       [11.209335, 11.140253, 51.472204],
                       [10.240910, 10.452121, 50.159449],
                       [ 9.976946,  9.554726, 52.018512],
                       [10.663980,  8.903366, 51.853128]]
    ethanol="""9
    Ethanol
    C     9.136479   11.724970   51.500927 C1
    H     9.294462   12.628635   50.910741 H1
    H     8.160480   11.311733   51.245960 H2
    H     9.126987   11.998458   52.556137 H3
    C    10.230421   10.715737   51.222941 C2
    H    11.209335   11.140253   51.472204 H4
    H    10.240910   10.452121   50.159449 H5
    O     9.976946    9.554726   52.018512 O
    H    10.663980    8.903366   51.853128 H6
    """
    molecule = dp.Molecule(atoms=ethanol, charge=+4)
    assert isinstance(molecule.charge, int)
    assert molecule.charge == 4
    assert isinstance(molecule.elements, list)
    for element in molecule.elements:
        assert isinstance(element, str)
    assert molecule.elements == ref_elements
    assert molecule.labels == ref_names
    assert isinstance(molecule.coordinates, np.ndarray)
    for coordinate in molecule.coordinates:
        assert isinstance(coordinate, np.ndarray)
        for value in coordinate:
            assert isinstance(value, float)
    for test_coordinate, ref_coordinate in zip(molecule.coordinates, ref_coordinates):
        assert pytest.approx(test_coordinate.tolist()) == ref_coordinate

def test_molecule_init_xyz():
    ref_elements = ['C', 'H', 'H', 'H', 'C', 'H', 'H', 'O', 'H']
    ref_names = ['C1', 'H1', 'H2', 'H3', 'C2', 'H4', 'H5', 'O', 'H6']
    ref_coordinates = [[ 9.136479, 11.724970, 51.500927],
                       [ 9.294462, 12.628635, 50.910741],
                       [ 8.160480, 11.311733, 51.245960],
                       [ 9.126987, 11.998458, 52.556137],
                       [10.230421, 10.715737, 51.222941],
                       [11.209335, 11.140253, 51.472204],
                       [10.240910, 10.452121, 50.159449],
                       [ 9.976946,  9.554726, 52.018512],
                       [10.663980,  8.903366, 51.853128]]
    molecule_dir = f'{os.path.dirname(__file__)}'
    molecule = dp.Molecule(xyz=f'{molecule_dir}/../sample_molecules/ethanol.xyz', charge=+4)
    assert isinstance(molecule.charge, int)
    assert molecule.charge == 4
    assert isinstance(molecule.elements, list)
    for element in molecule.elements:
        assert isinstance(element, str)
    assert molecule.elements == ref_elements
    assert molecule.labels == ref_names
    assert isinstance(molecule.coordinates, np.ndarray)
    for coordinate in molecule.coordinates:
        assert isinstance(coordinate, np.ndarray)
        for value in coordinate:
            assert isinstance(value, float)
    for test_coordinate, ref_coordinate in zip(molecule.coordinates, ref_coordinates):
        assert pytest.approx(test_coordinate.tolist()) == ref_coordinate

def test_molecule_init_exceptions():
    with pytest.raises(TypeError):
        dp.Molecule(charge=0.0)
    with pytest.raises(ValueError):
        atoms = "O 1.0 2.0 0.5 O1: H 0.0 1.0 1.0 H1: H -1.0 0.5 4.0 H2"
        dp.Molecule(atoms=atoms)
    with pytest.raises(ValueError):
        atoms = "O 1.0 2.0"
        dp.Molecule(atoms=atoms)
    with pytest.raises(ValueError):
        atoms = "O 1.0 2.0 0.5 O1 O2"
        dp.Molecule(atoms=atoms)
    with pytest.raises(ValueError):
        atoms = "O 1.0 2.0 0.5; Xy 0.0 0.0 0.0"
        dp.Molecule(atoms=atoms)
    with pytest.raises(TypeError):
        atoms = ["O 1.0 2.0 0.5 O1: H 0.0 1.0 1.0 H1: H -1.0 0.5 4.0 H2"]
        dp.Molecule(atoms=atoms)
    with pytest.raises(SyntaxError):
        dp.Molecule(atoms="O 0.0 0.0 0.0", xyz="test.xyz")


def test_molecule_xyz():
    ref_elements = ['O', 'H', 'H']
    ref_names = ['O', 'H', 'H']
    ref_coordinates = [[0.000000, 0.000000, 0.000000],
                       [0.758602, 0.000000, 0.504284],
                       [0.758602, 0.000000, -0.504284]]
    molecule_dir = f'{os.path.dirname(__file__)}'
    molecule = dp.Molecule()
    assert isinstance(molecule.charge, int)
    assert molecule.charge == 0
    molecule.xyz(f'{molecule_dir}/../sample_molecules/water.xyz')
    assert isinstance(molecule.elements, list)
    for element in molecule.elements:
        assert isinstance(element, str)
    assert molecule.elements == ref_elements
    assert molecule.labels == ref_names
    assert isinstance(molecule.coordinates, np.ndarray)
    for coordinate in molecule.coordinates:
        assert isinstance(coordinate, np.ndarray)
        for value in coordinate:
            assert isinstance(value, float)
    for test_coordinate, ref_coordinate in zip(molecule.coordinates, ref_coordinates):
        assert pytest.approx(test_coordinate.tolist()) == ref_coordinate


def test_molecule_xyz_exception():
    molecule = dp.Molecule()
    with pytest.raises(FileNotFoundError):
        molecule.xyz('test.xyz')


def test_molecule_num_atoms():
    atoms = "O 1.0 2.0 0.5; H 0.0 1.0 1.0; H -1.0 0.5 4.0"
    molecule = dp.Molecule(atoms=atoms)
    assert molecule.num_atoms == 3
