import daltonproject as dp


def test_property_init():
    properties = dp.Property(energy=True, gradient=True, hessian=True, geometry_optimization=True,
                             excitation_energy=True)
    assert "energy" in properties.settings
    assert properties.settings['energy']
    assert "gradient" in properties.settings
    assert properties.settings['gradient'] == "analytic"
    assert "hessian" in properties.settings
    assert properties.settings['hessian'] == "analytic"
    assert "geometry_optimization" in properties.settings
    assert properties.settings['geometry_optimization'] == "BFGS"
    assert 'excitation_energy' in properties.settings
    assert properties.settings['excitation_energy'] == 5


def test_property_energy():
    properties = dp.Property()
    properties.energy()
    assert "energy" in properties.settings
    assert properties.settings['energy']


def test_property_gradient():
    properties = dp.Property()
    properties.gradient(gradient_type='numeric')
    assert "gradient" in properties.settings
    assert properties.settings['gradient'] == "numeric"


def test_property_hessian():
    properties = dp.Property()
    properties.hessian(hessian_type='numeric')
    assert "hessian" in properties.settings
    assert properties.settings['hessian'] == "numeric"


def test_property_geometry_optimization():
    properties = dp.Property()
    properties.geometry_optimization(method='DFP')
    assert "geometry_optimization" in properties.settings
    assert properties.settings['geometry_optimization'] == "DFP"


def test_property_excitation_energies():
    properties = dp.Property()
    properties.excitation_energy(states=2, triplet=True)
    assert 'excitation_energy' in properties.settings
    assert properties.settings['excitation_energy'] == 2
    assert properties.settings['excitation_energy_triplet']
