from .dalton_ifc import DaltonIFC
from .lsdalton_ifc import LSDaltonIFC

programs = {'dalton': DaltonIFC, 'lsdalton': LSDaltonIFC}


def QCProgram(program):
    if not isinstance(program, str):
        raise TypeError('Program must be given as a string')
    if program.lower() not in programs.keys():
        raise ValueError(f'Program {program} is not supported')
    return programs[program.lower()]()
