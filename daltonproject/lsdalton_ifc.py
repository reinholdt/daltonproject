import re
import uuid
import subprocess as sp
import numpy as np
from .qcmethod import QCMethod
from .basis import Basis
from .property import Property
from .molecule import Molecule, ELEMENTS


def get_atom_basis(basis, num_atoms, labels):
    """Process basis set input which can be either a string with a single basis set name or a dictionary mapping
    atom labels to basis set"""
    atom_basis = None
    if isinstance(basis, str):
        atom_basis = [basis] * num_atoms
    elif isinstance(basis, dict):
        atom_basis = []
        for name in labels:
            try:
                atom_basis.append(basis[name])
            except KeyError:
                raise KeyError(f'No basis for {name} was provided')
    return atom_basis


def write_molecule(molecule, basis, filename):
    """Write Dalton/LSDalton molecule file"""
    atom_bases = get_atom_basis(basis.basis, molecule.num_atoms, molecule.labels)
    atom_types = 0
    coordinate_groups = []
    coordinate_group = []
    element_group = []
    basis_group = []
    previous_element = None
    previous_basis = None
    for element, coordinate, atom_basis in zip(molecule.elements, molecule.coordinates, atom_bases):
        if element != previous_element or atom_basis != previous_basis:
            coordinate_group = [coordinate]
            coordinate_groups.append(coordinate_group)
            element_group.append(element)
            basis_group.append(atom_basis)
            atom_types += 1
        else:
            coordinate_group.append(coordinate)
        previous_element = element
        previous_basis = atom_basis
    molecule_input = ''
    molecule_input += 'ATOMBASIS\n'
    molecule_input += 'Generated by Dalton Project\n'
    molecule_input += '\n'
    molecule_input += f'AtomTypes={atom_types} Charge={molecule.charge} NoSymmetry Angstrom\n'
    for basis, element, coordinate_group in zip(basis_group, element_group, coordinate_groups):
        molecule_input += f'Charge={ELEMENTS.index(element):.1f} Atoms={len(coordinate_group)} Basis={basis}\n'
        for coordinate in coordinate_group:
            molecule_input += f'{element:2} {coordinate[0]:12.6f} {coordinate[1]:12.6f} {coordinate[2]:12.6f}\n'
    with open('{0}.mol'.format(filename), 'w') as mol_file:
        mol_file.write(molecule_input)


def write_lsdalton_input(qc_method, properties, filename):
    # **GENERAL
    dalton_input = '**GENERAL\n'
    dalton_input += '.NOGCBASIS\n'

    # **WAVE FUNCTION
    wave_function = "**WAVE FUNCTION\n"
    if qc_method.settings['qc_method'] == "HF":
        wave_function += ".HF\n"
    elif qc_method.settings['qc_method'] == "DFT":
        wave_function += ".DFT\n"
        wave_function += f"{qc_method.settings['xc_functional']}\n"
    dalton_input += wave_function
 
    # **REPSONS
    response = ""
    if "gradient" in properties.settings.keys():
        if response == "":
            response += "**RESPONS\n"
        response += "*MOLGRA\n"
    dalton_input += response

    # **OPTIMIZE
    geometry_optimize = ""
    if "geometry_optimization" in properties.settings.keys():
        geometry_optimize = '**OPTIMIZE\n'
    dalton_input += geometry_optimize

    #*END OF INPUT
    dalton_input += "*END OF INPUT\n"

    with open('{0}.dal'.format(filename), 'w') as dal_file:
        dal_file.write(dalton_input)


class LSDaltonIFC:

    @staticmethod
    def compute(qc_method, basis, molecule, properties):
        if not isinstance(qc_method, QCMethod):
            raise TypeError
        if not isinstance(basis, Basis):
            raise TypeError
        if not isinstance(properties, Property):
            raise TypeError
        if not isinstance(molecule, Molecule):
            raise TypeError
        filename = f'{uuid.uuid4().hex}'
        write_molecule(molecule, basis, filename)
        write_lsdalton_input(qc_method, properties, filename)
        command = ['lsdalton', '-mol', f'{filename}.mol', '-dal', f'{filename}.dal', '-o',
                   f'{filename}.out']

        #For geometry optimization, keep also the xyz file of the converged geometry
        if "geometry_optimization" in properties.settings.keys():
            command += ['-get','molecule_out.xyz']

        result = sp.run(command, capture_output=True)

        try:
            result.check_returncode()
        except sp.CalledProcessError:
            print(result.stderr)
        return f'{filename}'

    @staticmethod
    def energy(output):
        with open(f'{output}.out', 'r') as output_file:
            match = re.findall(pattern=r'\n.*Final.*energy: +(.*)\n', string=output_file.read())
        if match is None:
            raise Exception(f'Energy not found in output: {output}')
        return float(match[-1])

    @staticmethod
    def electronic_energy(output):
        with open(f'{output}.out', 'r') as output_file:
            match = re.findall(pattern=r'\n.*Electronic energy: +(.*)\n', string=output_file.read())
        if match is None:
            raise Exception(f'Electronic energy not found in output: {output}')
        return float(match[-1])

    @staticmethod
    def nuclear_repulsion_energy(output):
        with open(f'{output}.out', 'r') as output_file:
            match = re.findall(pattern=r'\n.*Nuclear repulsion: +(.*)\n', string=output_file.read())
        if match is None:
            raise Exception(f'Nuclear repulsion energy not found in output: {output}')
        return float(match[-1])

    @staticmethod
    def final_geometry(output):
        with open(f'{output}.molecule_out.xyz', 'r') as output_file:
            output = output_file.read()
        return output
