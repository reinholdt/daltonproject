class QCMethod:
    """Specify the QC method including all associated settings."""
    def __init__(self, qc_method, xc_functional=None):
        self.settings = {}
        self.qc_method(qc_method=qc_method)
        if xc_functional is not None and qc_method != 'DFT':
            raise TypeError('Specifying XC functional is only valid together with DFT')
        if xc_functional is not None and qc_method == 'DFT':
            self.xc_functional(xc_functional)

    def qc_method(self, qc_method):
        if not isinstance(qc_method, str):
            raise TypeError('QC method must be given as a string')
        self.settings.update({'qc_method': qc_method.upper()})

    def xc_functional(self, xc_functional):
        if not isinstance(xc_functional, str):
            raise TypeError('XC functional must be given as a string')
        self.settings.update({'xc_functional': xc_functional.upper()})

    def scf_threshold(self, threshold):
        if not isinstance(threshold, float):
            raise TypeError('SCF threshold must be given as a float')
        self.settings.update({'scf_threshold': threshold})
