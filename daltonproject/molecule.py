import numpy as np

ELEMENTS = ('X', 'H', 'He', 'Li', 'Be', 'B', 'C', 'N', 'O', 'F', 'Ne', 'Na', 'Mg', 'Al', 'Si', 'P', 'S',
            'Cl', 'Ar', 'K', 'Ca', 'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 'Ga',
            'Ge', 'As', 'Se', 'Br', 'Kr', 'Rb', 'Sr', 'Y', 'Zr', 'Nb', 'Mo', 'Tc', 'Ru', 'Rh',
            'Pd', 'Ag', 'Cd', 'In', 'Sn', 'Sb', 'Te', 'I', 'Xe', 'Cs', 'Ba', 'La', 'Ce', 'Pr',
            'Nd', 'Pm', 'Sm', 'Eu', 'Gd', 'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Lu', 'Hf', 'Ta',
            'W', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', 'Tl', 'Pb', 'Bi', 'Po', 'At', 'Rn', 'Fr',
            'Ra', 'Ac', 'Th', 'Pa', 'U', 'Np', 'Pu', 'Am', 'Cm', 'Bk', 'Cf', 'Es', 'Fm', 'Md',
            'No', 'Lr', 'Rf', 'Db', 'Sg', 'Bh', 'Hs', 'Mt')


class Molecule:
    """Molecule"""

    def __init__(self, atoms=None, charge=0, xyz=None):
        if not isinstance(charge, int):
            raise TypeError('Charge must be an integer')
        self.charge = charge
        self.elements = None
        self.labels = None
        self.coordinates = None
        if atoms is not None and xyz is not None:
            raise SyntaxError('Specify either atoms or xyz')
        if xyz is not None:
            self.xyz(xyz)
        if atoms is not None:
            self.atoms(atoms)

    # Allows both atom-coordinate block or xyz format input
    def atoms(self, atoms):
        if not isinstance(atoms, str):
            raise TypeError("Atoms are given as a string using either '\\n' or ';' to separate atoms")
        if ";" in atoms:
            lines = atoms.split(";")
        elif "\n" in atoms:
            lines = atoms.split("\n")
        elif not 4 <= len(atoms.split()) <= 5:
            raise ValueError(f"Incompatible input: {atoms}")
        else:
            # assume it is a single atom
            lines = [atoms]

        # Remove two first lines if the format is xyz, i.e. that the first line contains 
        # number of atoms (and implicitly that second line is then taken to be a comment line)
        if len(lines[0].split()) == 1:
            if int(lines[0]) <= len(lines) - int(2):
                lines = lines[2:-1]
        
        elements = []
        labels = []
        coordinates = []
        for line in lines:
            atom = line.split()
            element = atom[0].strip().title()
            if element not in ELEMENTS:
                raise ValueError('Unknown element: {element}')
            elements.append(element)
            coordinates.append(np.array([float(value) for value in atom[1:4]]))
            if len(atom) == 5:
                labels.append(atom[4].strip())
            else:
                labels.append(atom[0].strip())
        self.elements = elements
        self.labels = labels
        self.coordinates = np.array(coordinates)

    def xyz(self, filename):
        with open(f'{filename}', 'r') as xyzfile:
            lines = xyzfile.read()
        self.atoms(lines)

    @property
    def num_atoms(self):
        return len(self.elements)
