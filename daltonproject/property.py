class Property:
    """Define settings to be computed"""
    def __init__(self, energy=False, gradient=False, hessian=False, geometry_optimization=False,
                 excitation_energy=False):
        self.settings = {}
        if energy:
            self.energy()
        if gradient:
            self.gradient()
        if hessian:
            self.hessian()
        if geometry_optimization:
            self.geometry_optimization()
        if excitation_energy:
            self.excitation_energy()

    def energy(self):
        self.settings.update({'energy': True})

    def gradient(self, gradient_type='analytic'):
        self.settings.update({'gradient': gradient_type})

    def hessian(self, hessian_type='analytic'):
        self.settings.update({'hessian': hessian_type})

    def geometry_optimization(self, method='BFGS'):
        self.settings.update({'geometry_optimization': method})

    def excitation_energy(self, states=5, triplet=False):
        """Compute excitation energies"""
        self.settings.update({'excitation_energy': states})
        if triplet:
            self.settings.update({'excitation_energy_triplet': triplet})

