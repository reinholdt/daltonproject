class Basis:
    """Specify the AO basis
    The AO basis set can either be given as:
      1. a basis set name, indicating the same basis set will be used for all atoms, or
      2. a dictionary with label and basis set name as key and value, respectively.
    """
    def __init__(self, basis):
        if not isinstance(basis, str) and not isinstance(basis, dict):
            raise TypeError(f'Unsupported basis set type: {type(basis)}')
        if isinstance(basis, dict):
            for key, value in basis.items():
                if not isinstance(key, str):
                    raise TypeError(f'Invalid atom label type: {type(key)}')
                if not isinstance(value, str):
                    raise TypeError(f'Unknown basis type: {type(value)}')
        self.basis = basis
