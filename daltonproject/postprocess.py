class PostProcess:

    def __init__(self, program, output):
        self.program = program
        self.output = output

    @property
    def energy(self):
        return self.program.energy(self.output)

    @property
    def electronic_energy(self):
        return self.program.electronic_energy(self.output)

    @property
    def nuclear_repulsion_energy(self):
        return self.program.nuclear_repulsion_energy(self.output)

    @property
    def num_electrons(self):
        return self.program.num_electrons(self.output)

    @property
    def num_orbitals(self):
        return self.program.num_orbitals(self.output)

    @property
    def mo_energies(self):
        return self.program.mo_energies(self.output)

    @property
    def final_geometry(self):
        return final_geometry(self.output)
